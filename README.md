# TicTac Toe Challenge #

The game is a basic tic tac toe game with board size of N and using CI and Mongo DB to save game sessions and data.

### Requirements ###

* PHP 5 or higher

* Apache 2 or higher

* Mongo DB (it should be installed as PHP extension)

### Page Styling ###

You edit SASS file to update styling.

### How to Install Mongo on Ubunto ###

Run the following commands:


```
#!unix

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
```

```
#!unix

echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
```

```
#!unix

sudo apt-get update
```

```
#!unix

sudo apt-get install -y mongodb-org
```

```
#!unix

sudo apt-get install -y mongodb-org=2.6.1 mongodb-org-server=2.6.1 mongodb-org-shell=2.6.1 mongodb-org-mongos=2.6.1 mongodb-org-tools=2.6.1
```

```
#!unix

sudo service mongod start
```


### How to Install Mongo as PHP Extension ###


```
#!unix

sudo apt-get install php5-mongo
```

### Mongo Administrator ###

I had added mongo administrator to see the DBs.

If you already setup the site for the game say for example it can be access via http://game.localhost

Mongo Administrator can be found on http://game.localhost/mongoadmin

There is no configuration update once your Mongo instance is running unless your Mongo installation has authentication. In case of the Mongo setup with authentication just edit the index.php under mongoadmin on the root folder of the game. Adjust the configuration to the correct details based on your setup.