App = {
    onReady: function()
    {
        App.playerMove();
        App.tileHover();
    },
    dialogBox: function(options,callback)
    {
        var settings = $.extend({
            dialog_text:        'Add text here.',
            dialog_title:       'Modal Title',
            dialog_type:        'confirm'
        }, options);

        $("#dialog-box").html(settings.dialog_text);

        if (settings.dialog_type == 'confirm') {
            // Define the Dialog and its properties.
            $("#dialog-box").dialog({
                resizable: false,
                modal: true,
                title: settings.dialog_title,
                height: 'auto',
                width: 400,
                buttons: {
                    "Yes": function () {
                        $(this).dialog('close');
                        callback(true);
                    },
                    "No": function () {
                        $(this).dialog('close');
                        callback(false);
                    }
                }
            });
        } else {
            // Define the Dialog and its properties.
            $("#dialog-box").dialog({
                resizable: false,
                modal: true,
                title: settings.dialog_title,
                height: 'auto',
                width: 400,
                buttons: {
                    "OK": function () {
                        $(this).dialog('close');
                        callback(true);
                    }
                }
            });
        }
    },
    sendData: function(options,callback)
    {
        var settings = $.extend({
            path: '',
            data: ''
        }, options);
        $.post( settings.path, settings.data ).done(function( data ) {
            callback( data );
        });
    },
    playerMove: function()
    {
        if ( $('#board').length > 0 ) {
            $('.tiles').click(function(){
                var self = this;
                if ($(self).find('i').hasClass('taken')) {
                    App.dialogBox({
                        dialog_text: 'Invalid move! Tile is already taken.',
                        dialog_title: 'Not Allowed',
                        dialog_type: 'alert'
                    },function(status){});
                } else {
                    App.dialogBox({
                        dialog_text: 'Are you sure with this move?',
                        dialog_title: 'Move Confirmation'
                    },function(status){
                        if (status) {
                            App.disableHover();
                            App.sendData({
                                path: base_url + 'control/move',
                                data: { 'row': $(self).data('row'), 'column': $(self).data('column'), 'player': $('#player').attr('value') }
                            },function(data){
                                try
                                {
                                    var data = $.parseJSON(data);
                                    if (data.saved != true) {
                                        App.dialogBox({
                                            dialog_text: 'Invalid move! Tile is already taken.',
                                            dialog_title: 'Not Allowed',
                                            dialog_type: 'alert'
                                        },function(status){});
                                    } else {
                                        if (data.result.status == true ) {
                                                App.dialogBox({
                                                    dialog_text: 'Game is over. Player <b>'+ (data.result.player == '1' ? 'X' : 'O') +'</b> is the winner.',
                                                    dialog_title: 'Game Over',
                                                    dialog_type: 'alert'
                                                },function(){
                                                    window.location = base_url + 'control/newgame';
                                                });
                                        } else { 
                                            if (data.result.status == 'tie') {
                                                App.dialogBox({
                                                    dialog_text: 'Game is over. We have a <b>TIE</b>.',
                                                    dialog_title: 'Game Over',
                                                    dialog_type: 'alert'
                                                },function(){
                                                    window.location = base_url + 'control/newgame';
                                                });
                                            } else {
                                                $(self).html(data.update.icon);
                                                $('#player').attr('value',data.update.player);
                                                $('#whosplayer b').html( data.update.player == 1 ? 'X' : 'O' );
                                                $(self).addClass('taken');
                                                App.enableHover();
                                            }
                                        }
                                    }
                                }
                                catch(e)
                                {
                                   console.log('Invalid response.');
                                }
                            });
                        }
                    });
                }
            });
        }
    },
    disableHover: function()
    {
        $('.player-icons').addClass('disable');
    },
    enableHover: function()
    {
        $('.player-icons').removeClass('disable');
    },
    tileHover: function()
    {
        var current_player;
        if ( $('#board').length > 0 ) {
            $('.tiles').mouseenter(function(){
                current_player = $('#player').attr('value');
                $(this).find('.player-icons').stop(true,false).fadeOut('fast');
                $(this).find('.player'+current_player).not('.disable').fadeIn('fast');
            }).mouseleave(function(){
                $(this).find('.player-icons').stop(true,false).fadeOut('fast');
            });
        }
    }

};

$(document).ready(App.onReady);