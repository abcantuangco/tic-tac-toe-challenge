<?php
    //list js here
    if ($footer_js) {
        foreach ($footer_js as $js):
            $js_data = explode(':',$js);
            if (@$js_data[1]) {
                echo '<script type="text/javascript" src="'.base_url().$js_data[0].'"></script>'."\n";
            } else {
                echo '<script type="text/javascript" src="'. base_url() .'assets/js/'.$js.'"></script>'."\n";
            }
        endforeach;
    }
?>
</body>
</html>