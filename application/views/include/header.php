<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title><?php echo  $title;?></title>
    <meta name="description" content="<?php echo  $description;?>">
    <meta name="author" content="<?php echo  $author;?>">
    <?php
        // list css here
        if ($header_style)
        {
            foreach ($header_style as $style):
                $style_data = explode(':',$style);
                if (@$style_data[1]) {
                    echo '<link rel="stylesheet" type="text/css" href="'.base_url().$style_data[0].'"/>' . "\n";
                } else {
                    echo '<link rel="stylesheet" type="text/css" href="'. base_url() .'assets/stylesheets/'.$style.'"/>' . "\n";
                }

            endforeach;
        }
    ?>
    <script type="text/javascript"> 
        base_url    = '<?php echo base_url(); ?>';
    </script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>