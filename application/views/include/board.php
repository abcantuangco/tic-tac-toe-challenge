<div class="row">
    <div class="columns">
        <h1>Tic Tac Toe Challenge</h1>
        <h3>Please click on the tiles when it is your turn.</h3>
        <div id="board">
            <div class="tiles"><i class="fa fa-circle-o"></i></div>
            <div class="tiles"><i class="fa fa-times"></i></div>
            <div class="tiles"><i class="fa fa-circle-o"></i></div>
            <div class="tiles"><i class="fa fa-times"></i></div>
            <div class="tiles"><i class="fa fa-circle-o"></i></div>
            <div class="tiles"><i class="fa fa-times"></i></div>
            <div class="tiles"><i class="fa fa-circle-o"></i></div>
            <div class="tiles"><i class="fa fa-times"></i></div>
            <div class="tiles"><i class="fa fa-circle-o"></i></div>
        </div>
        <div id="button">
            <a href="javascript:void(0)">Take Turn</a>
        </div>
    </div>
</div>