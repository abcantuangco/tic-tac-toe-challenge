<?php 
    // include header
    $this->load->view('include/header');
?>
<div class="row full-width">
    <div class="columns">
        <h1>Tic Tac Toe Challenge</h1>
        <h2 id="message" style="display:none;"><span></span></h2>
        <h3>Please click on the tiles when it is your turn. <a href="/control/newgame">Click here</a> for new game.</h3>
        <?php echo $this->game->createBoard(); ?>
        <div id="button">
            <span id="whosplayer">It's <b><?php echo ($this->game->player == 1 ? 'X' : 'O'); ?></b>'s turn.</a>
            <input type="hidden" value="<?php echo $this->game->player; ?>" id="player"/>
        </div>
    </div>
</div>

<div id="dialog-box"></div>

<?php
    // include the footer
    $this->load->view('include/footer');
 ?>