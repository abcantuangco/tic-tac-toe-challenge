<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Game library for tictactoe of any board size using Mongo DB to save game data at runtime.
*
* @package     CodeIgniter
* @subpackage  Libraries
* @category    Game
* @author      Arsenio B. Cantuangco Jr. <abcantuangco@gmail.com>
*/

class Game {

    
    public      $player;
    protected   $game_session;
    protected   $board;
    protected   $CI;
    protected   $collection_name    = 'tictactoe';
    protected   $win_moves          = 3;
    
    protected   $session;
    protected   $mongolib;
    protected   $custom_security;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->library('session');
        $this->CI->session;
        $this->CI->load->library('mongolib');
        $this->CI->mongolib;
        $this->CI->load->library('custom_security');
        $this->CI->custom_security;
        $this->game_session = $this->CI->session->userdata('game_session');
    }

    /**
     * Start Game
     * Initialize game parameters, create game session and save board size to cookie
     * Minimum board size is 3 by 3
     * @param  integer $board_size [description]
     * @return none
     */
    public function start($board_size = 3)
    {
         if ( $this->checkIfGameIsOnGoing() == false )
         {
            $this->player   = rand(1,2);
            $this->newSession();
            $this->CI->session->set_userdata(array(
                'board_size'=>$board_size
                ));
         } else {
            $this->getPlayer();
         }
    }

    /**
     * Get Board Size
     * Get the board size based on initialization
     * @return board size
     */
    public function getBoardSize()
    {
        return $this->CI->session->userdata('board_size');
    }
    
    /**
     * Get Player
     * Return the next player based on the database data
     * @return next player
     */
    private function getPlayer() {
        $result = $this->CI->mongolib->get_where($this->collection_name,array(
                        'session'   => $this->game_session
                    ));
        foreach( $result as $doc ) {
                $player = $doc['player'];
            }
        $this->player = ( $player == 1 ? 2 : 1 );
    }
    
    /**
     * Create Session
     * Create game session and save it to cookie
     * @return game session
     */
    public function newSession()
    {
        $this->removeGameSession();

        $this->game_session = strtotime(date('Y-m-d G:i:s'));
        // set the game session so it can be called globally
        $this->CI->session->set_userdata(array('game_session'=>$this->game_session));
    }

    /**
     * Game Session Log
     * Save game session log
     * @param  string $method
     * @return none
     */
    private function gameSessionLog($method = 'add')
    {
        if ($method == 'add') {
            $this->CI->mongolib->insert($this->collection_name,array(
                'game_session_log'  => $this->game_session,
                'created'       => strtotime(date('Y-m-d G:i:s')),
                'updated'       => 0
            ));
        } else {
            $this->CI->mongolib->where(array('game_session_log'=>$this->game_session));
            $this->CI->mongolib->update($this->collection_name, array('updated' => strtotime(date('Y-m-d G:i:s'))));
        }
    }

    /**
     * Check the winner
     * Check winner based on passed data every player move
     * @param  array $data
     * @return array winning data
     */
    public function checkWinner($data)
    {
        $result         = array();
        $board_size     = $this->getBoardSize();
        $this->gameSessionLog('update');

        //check col +
        for($i = 1; $i <= $this->win_moves; $i++){
            $column     = (string)($data['column'] + $i);
            $mod_data   = array_merge($data, array('column' => $column));
            if($column > $board_size)
                break;
            if($this->checkMove($mod_data) == false)
                break;
            if($i == ($this->win_moves-1)){
                $result = array('status'=>true,'player'=>$data['player'],'details'=>'win by column +');
            }
        }
        //check col -
        for($i = 1; $i <= $this->win_moves; $i++){
            $column     = (string)($data['column'] - $i);
            $mod_data   = array_merge($data, array('column'=>$column));
            if($column < 1)
                break;
            if($this->checkMove($mod_data) == false)
                break;
            if($i == ($this->win_moves-1)){
                $result = array('status'=>true,'player'=>$data['player'],'details'=>'win by column -');
            }
        }
        //check col -1 and +1
        $column     = (string)($data['column'] - 1);
        $mod_data   = array_merge($data, array('column'=>$column));
        $minus_move = $this->checkMove($mod_data);
        $column     = (string)($data['column'] + 1);
        $mod_data   = array_merge($data, array('column'=>$column));
        $plus_move  = $this->checkMove($mod_data);
        if ( $minus_move && $plus_move )
            $result = array('status'=>true,'player'=>$data['player'],'details'=>'win by column + or -');

        //check row +
        for($i = 1; $i <= $this->win_moves; $i++){
            $row        = (string)($data['row'] + $i);
            $mod_data   = array_merge($data, array('row' => $row));
            if($row > $board_size)
                break;
            if($this->checkMove($mod_data) == false)
                break;
            if($i == ($this->win_moves-1)){
                $result = array('status'=>true,'player'=>$data['player'],'details'=>'win by row +');
            }
        }
        //check row -
        for($i = 1; $i <= $this->win_moves; $i++){
            $row        = (string)($data['row'] - $i);
            $mod_data   = array_merge($data, array('row'=>$row));
            if($row < 1)
                break;
            if($this->checkMove($mod_data) == false)
                break;
            if($i == ($this->win_moves-1)){
                $result = array('status'=>true,'player'=>$data['player'],'details'=>'win by row -');
            }
        }
        //check row -1 and +1
        $row     = (string)($data['row'] - 1);
        $mod_data   = array_merge($data, array('row'=>$row));
        $minus_move = $this->checkMove($mod_data);
        $row     = (string)($data['row'] + 1);
        $mod_data   = array_merge($data, array('row'=>$row));
        $plus_move  = $this->checkMove($mod_data);
        if ( $minus_move && $plus_move )
            $result = array('status'=>true,'player'=>$data['player'],'details'=>'win by row + or -');

        //check diagonal +
        for($i = 1; $i <= $this->win_moves; $i++){
            $row        = (string)($data['row'] + $i);
            $column     = (string)($data['column'] + $i);
            $mod_data = array_merge($data, array('column'=>$column,'row'=>$row));
            if(($row > $board_size) || ($column > $board_size))
                break;
            if($this->checkMove($mod_data) == false)
                break;
            if($i == ($this->win_moves-1)){
                $result = array('status'=>true,'player'=>$data['player'],'details'=>'win by diagonal +');
            }
        }
        //check diagonal -
        for($i = 1; $i <= $this->win_moves; $i++){
            $row        = (string)($data['row'] - $i);
            $column     = (string)($data['column'] - $i);
            $mod_data = array_merge($data, array('column'=>$column,'row'=>$row));
            if(($row > $board_size) || ($column > $board_size))
                break;
            if($this->checkMove($mod_data) == false)
                break;
            if($i == ($this->win_moves-1)){
                $result = array('status'=>true,'player'=>$data['player'],'details'=>'win by diagonal -');
            }
        }
        //check diagonal -1 and +1
        $row        = (string)($data['row'] - 1);
        $column     = (string)($data['column'] - 1);
        $mod_data = array_merge($data, array('column'=>$column,'row'=>$row));
        $minus_move = $this->checkMove($mod_data);
        $row        = (string)($data['row'] + 1);
        $column     = (string)($data['column'] + 1);
        $mod_data = array_merge($data, array('column'=>$column,'row'=>$row));
        $plus_move  = $this->checkMove($mod_data);
        if ( $minus_move && $plus_move )
            $result = array('status'=>true,'player'=>$data['player'],'details'=>'win by diagonal + or -');
        
        //check anti-diagonal +
        for($i = 1; $i <= $this->win_moves; $i++){
            $row        = (string)($data['row'] + $i);
            $column     = (string)($data['column'] - $i);
            $mod_data = array_merge($data, array('column'=>$column,'row'=>$row));
            if(($row > $board_size) || ($column > $board_size))
                break;
            if($this->checkMove($mod_data) == false)
                break;
            if($i == ($this->win_moves-1)){
                $result = array('status'=>true,'player'=>$data['player'],'details'=>'win by anti-diagonal +');
            }
        }
        //check anti-diagonal -
        for($i = 1; $i <= $this->win_moves; $i++){
            $row        = (string)($data['row'] - $i);
            $column     = (string)($data['column'] + $i);
            $mod_data = array_merge($data, array('column'=>$column,'row'=>$row));
            if(($row > $board_size) || ($column > $board_size))
                break;
            if($this->checkMove($mod_data) == false)
                break;
            if($i == ($this->win_moves-1)){
                $result = array('status'=>true,'player'=>$data['player'],'details'=>'win by anti-diagonal +');
            }
        }
        //check anti-diagonal -1 and +1
        $row        = (string)($data['row'] + 1);
        $column     = (string)($data['column'] - 1);
        $mod_data = array_merge($data, array('column'=>$column,'row'=>$row));
        $minus_move = $this->checkMove($mod_data);
        $row        = (string)($data['row'] - 1);
        $column     = (string)($data['column'] + 1);
        $mod_data = array_merge($data, array('column'=>$column,'row'=>$row));
        $plus_move  = $this->checkMove($mod_data);
        if ( $minus_move && $plus_move )
            $result = array('status'=>true,'player'=>$data['player'],'details'=>'win by anti-diagonal + or -');

        //check draw
        if (empty($result)) {
            if( $this->countMoves() == ($board_size*$board_size) ){
                $result = array('status'=>'tie','player'=>$data['player'],'details'=>'game is a tie');
            }
        }

        if (!empty($result)) {
            $this->saveGameLog($result);
        }

        return $result;
    }

    /**
     * Check Player Move
     * Check player move if exists in the database
     * @param  array $data
     * @return boolean true if it exist otherwise false
     */
    private function checkMove($data)
    {
        $this->CI->custom_security->inputFilter($data);
        $result = $this->CI->mongolib->get_where($this->collection_name,array(
                        'session'   => $this->game_session,
                        'row'       => $data['row'],
                        'column'    => $data['column'],
                        'player'    => $data['player']
                    ));
        $count = $result->count();
        return ($count > 0 ? true : false);
    }

    /**
     * Check if game is on going
     * On page refresh the page will be repopulated with data if game is on going
     * @return boolean true if there is an on going game otherwise false
     */
    private function checkIfGameIsOnGoing()
    {
        $result = $this->CI->mongolib->get_where($this->collection_name,array(
                        'session'   => $this->game_session
                    ));
        $count = $result->count();
        return ($count > 0 ? true : false); 
    }

    /**
     * Check if move is taken
     * @param  array $data
     * @return boolean true is move already existed otherwise false
     */
    private function checkIfMoveAlreadyTaken($data)
    {
        $this->CI->custom_security->inputFilter($data);
        $result = $this->CI->mongolib->get_where($this->collection_name,array(
                        'session'   => $this->game_session,
                        'row'       => $data['row'],
                        'column'    => $data['column']
                    ));
        $count = $result->count();
        return ($count > 0 ? true : false);
    }

    /**
     * Save player move
     * @param  array $data
     * @return boolean true if game is saved otherwise false
     */
    public function saveMove($data)
    {
        $processed = false;
        if ($this->checkIfMoveAlreadyTaken($data)==false)
        {
            $this->CI->custom_security->inputFilter($data);
            $this->CI->mongolib->insert($this->collection_name,array(
                'session'   => $this->game_session,
                'row'       => $data['row'],
                'column'    => $data['column'],
                'player'    => $data['player']
            ));
            $processed = true;
        } else {
            $processed = false;
        }
        return $processed;
    }

    /**
     * Save Game Log
     * Save game log once game is finished or someone wins
     * @param  array $data
     * @return none
     */
    private function saveGameLog($data)
    {
        $this->CI->custom_security->inputFilter($data);
        $this->CI->mongolib->insert($this->collection_name,array(
            'game_session'  => $this->game_session,
            'status'        => $data['status'],
            'player'        => $data['player'],
            'detail'        => $data['details']
        ));
    }
    
    /**
     * Count Moves
     * Count current game moves
     * @return integer number of moves done by both player
     */
    private function countMoves()
    {
        $result = $this->CI->mongolib->get_where($this->collection_name,array('session' => $this->game_session));
        $count = $result->count();
        return $count;
    }

    /**
     * Update Board Elements
     * Update board elements based on passed data
     * @param  array $data
     * @return array data to update board via javascript
     */
    public function updateBoardElements($data)
    {
        $this->CI->custom_security->inputFilter($data);
        $player     = ( $data['player'] == 1 ? 2 : 1 );
        $icon       = ( $data['player'] == 1 ? '<i class="fa fa-times"></i>' : '<i class="fa fa-circle-o"></i>' );
        $response = array('player'  => $player,
                          'icon'    => $icon,
                          'row'     => $data['row'],
                          'column'  => $data['column']);
        return $response;
    }

    /**
     * Remove game session
     * Remove data from the specified game session
     * @return none
     */
    private function removeGameSession()
    {
        $this->CI->mongolib->delete($this->collection_name,array('session' => $this->game_session));
        $this->CI->mongolib->delete($this->collection_name,array('game_session_log' => $this->game_session));
    }

    /**
     * Create game board
     * Create game board based on the existing data or if it is a newly created game
     * @return string the html data
     */
    public function createBoard()
    {
        $moves          = array();
        $player_icon    = false;
        if ( $this->checkIfGameIsOnGoing() != false )
        {
            $result = $this->CI->mongolib->get_where($this->collection_name,array('session' => $this->game_session));
            foreach( $result as $doc ) {
                switch($doc['player'])
                {
                    case 1:
                        $player_icon = '<i class="fa fa-times taken"></i>';
                    break;
                    case 2:
                        $player_icon = '<i class="fa fa-circle-o taken"></i>';
                    break;
                    default:
                        $player_icon = false;
                    break;
                }
                $moves[$doc['row']][$doc['column']] = $player_icon;
            }
        }
        
        $board = '<div id="board">';
        for ($i=1; $i <= $this->getBoardSize(); $i++) {
            $board .= '<div class="board-row">';
            for ($n=1; $n <= $this->getBoardSize(); $n++) { 
                $board .= '<div class="tiles board-tile" data-row="'.$i.'" data-column="'.$n.'">';
                if ( $player_icon ) {
                    if ( @$moves[$i][$n] ) {
                        $board  .= @$moves[$i][$n];
                    } else {
                        $board  .= '<span class="player1 player-icons" style="display:none;"><i class="fa fa-times"></i></span>';
                        $board  .= '<span class="player2 player-icons" style="display:none;"><i class="fa fa-circle-o"></i></span>';
                    }
                } else {
                    $board  .= '<span class="player1 player-icons" style="display:none;"><i class="fa fa-times"></i></span>';
                    $board  .= '<span class="player2 player-icons" style="display:none;"><i class="fa fa-circle-o"></i></span>';
                }
                $board .= '</div>';
            }
            $board .= '</div>';
        }
        $board .= '</div>';
        return $board;
    }
}

/* End of file Game.php */
/* Location: ./application/libraries/Game.php */