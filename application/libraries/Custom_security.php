<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custom_security {

    public function cleanInput($value){
        
    if (PHP_VERSION < 6) {
        $value = get_magic_quotes_gpc() ? stripslashes($value) : $value;
    } else {
        $value = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($value) : mysql_escape_string($value);    
    }
        $value = addslashes($value);
    return $value;
    }
    
    public function inputFilter($data) {

        $CI =& get_instance();
        $CI->load->helper('security');

        foreach($data as $key=>$value)
        {
            if ( is_array($value) ) {
              $data[$key] = $this->inputFilter($value);
            } else {
              $data[$key] = $this->cleanInput(xss_clean(trim($value)));
            }
        }
        return $data;
    }

}