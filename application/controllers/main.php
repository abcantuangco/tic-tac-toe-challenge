<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     */
    public function index()
    {
        $this->load->library('mongolib');
        $this->load->library('game');
        // default board size is 3(minimum board size), you can modify board size by specifying it like this $this->game->start(N) which N is any integer. 
        $this->game->start();
        $data['title']          = "Tic Tac Toe";
        $data['author']         = "Arsenio B. Cantuangco Jr. | abcantuangco@gmail.com";
        $data['description']    = "A simple tic tac toe game challenge.";
        $data['header_style']   = array('vendor/fortawesome/font-awesome/css/font-awesome.min.css:true','vendor/components/jqueryui/themes/smoothness/jquery-ui.min.css:true','styles.css');
        $data['footer_js']      = array('vendor/components/jquery/jquery.min.js:true','vendor/components/jqueryui/jquery-ui.min.js:true','app.js');
        $data['board_size']     = $this->game->getBoardSize(); 
        $this->load->view('index_page', $data);
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */