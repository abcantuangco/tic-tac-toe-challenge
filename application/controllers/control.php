<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Control extends CI_Controller {

    public function move()
    {
        $this->load->library('game');
        $is_saved           = $this->game->saveMove($_POST);
        $result             = $this->game->checkWinner($_POST);
        $update_data        = $this->game->updateBoardElements($_POST);
        $response           = array('saved'  => $is_saved,
                                    'result'    => $result,
                                    'update'    => $update_data );
        echo json_encode($response);
    }

    public function newGame()
    {
        $this->load->library('game');
        $this->game->newSession();
        redirect('/', 'refresh');
    }

}

/* End of file game.php */
/* Location: ./application/controllers/game.php */